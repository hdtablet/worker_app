// main app
const express = require('express')
const app = express()
const workerFarm = require('node-worker-farm')
const CronJob = require('cron').CronJob;
var neDB = require('nedb');
const db = new neDB({ filename: 'data/jobs.db', autoload: true });

db.persistence.setAutocompactionInterval(3600000)

function fastDate() {
    return new Date().toISOString() + "::"
}

async function getJob(id) {
    let data = await new Promise((resolve, reject) => {
        db.find( { "task": Number(id) } , (err, data) => {
            if (err){
                reject(err);
            }  
          //  console.log("FIND JOB "+id)
            resolve(data[0]);
        });
    });
    return data;
}

async function updateStatusJob(id,status){
    // var data = await getJob(id);
     await new Promise((resolve, reject) => {
        db.update({ "task": id }, { $set: {  "status" : status } }, {}, (err, num) => {
           if (err){
               reject(err);
           }  
           resolve(); 
        });
     }); 
}

function createWorker(data){
    console.log(fastDate() + ">CREATE WORKER "+data.task);
    const service = workerFarm(require.resolve(data.url))
    service(data.task, async function (err, output) {
       // console.log(fastDate() + ">>>RESULT TASK"+output)
        workerFarm.end(service)
        console.log(fastDate() + ">>>END WORKER "+output);
        await updateStatusJob(output,0);
    })
  //  console.log(fastDate() + ">CREATE WORKER END ");
}

 function createCron(datafast) {
    // cron wont start automatically
    task = new CronJob(datafast.cron, async function() {
      //  console.log(fastDate() + '>CRONJOB EXECUTE');
        try{

            var data = await getJob(1)
            if (data.status == 0) {
                await updateStatusJob(1,1);
                createWorker(data)
              //  console.log(fastDate() + ">TASK CALLED");
            } else {
                console.log(fastDate() + ">WAIT TASK");
            }
        }catch(e){
            console.log(fastDate() + '>CRONJOB ERROR',e);
        }
       
    });

    task.start()
}

app.get('/start', async function(req, res) {
    var datafast  = await getJob(1)
  //  updateStatusJob(1,req.query.id_job);
    createCron(datafast);
    res.send('start process')
})

app.get('/stop', (req, res) => {
    task.stop()
    res.send('stop process')
})

app.listen(process.env.PORT || 3031)

console.log('app is running!')

module.exports = app


// async function  simulateData (){
//    await db.insert(
//        [
//             { task: 1 , url : "./testScript.js" , call_task : 2 , "cron" : "*/10 * * * * *", "status" : "0" }, 
//             { task: 2 , url : "./testScript2.js", "status" : "0" }

//        ], function (err, newDocs) {
      
//    });
// }
// simulateData();
