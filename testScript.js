function fastDate() {
    return new Date().toISOString() + "::"
}

module.exports = function (id ,callback = (err, result) => ({})) {

    try {

       // console.log(fastDate() + ">>>TASK EXCUTE "+id)

        setTimeout(function () {
                callback(null, id)
                console.log(fastDate() + ">>>TASK completed "+id)
            },
             45000);

    } catch (ex) {
        callback(ex)
        console.error(ex)
    }

}